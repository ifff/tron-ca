with (import <nixpkgs> {});
stdenv.mkDerivation rec {
   name = "tron-ca";
   buildInputs = [
     gcc
     gnumake
     SDL2
   ];
}
