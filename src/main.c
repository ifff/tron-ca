/**
 * This block cellular automaton has cells partitioned into 2x2 blocks
 * (aka Margolus neighborhoods). The blocks shift down by 1 cell on each axis each iteration.
 * If a block's cells are all the same state, the cells reverse. Otherwise, the cells remain
 * the same.
 */

#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

const int SCREEN_WIDTH = 500;
const int SCREEN_HEIGHT = 500;
const int CELL_WIDTH = 2;
const int CELL_HEIGHT = 2;
const int COLS = 250;
const int ROWS = 250;
const int MAX_ITERATIONS = 15;

typedef enum {
							CELLTYPE_DEAD,
							CELLTYPE_LIVE,
} CellType;

void renderCell(CellType ctype, int x, int y, SDL_Renderer* ren);
CellType* doRule(CellType cells[COLS][ROWS], int x, int y);

int main() {
  SDL_Window* win = NULL;
  SDL_Renderer* ren;
  SDL_Event event;
  CellType cells[COLS][ROWS];
  int blockOffset = 0;
  int iterations = 0;
  
  // initialize SDL
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    printf("SDL had an error: %s\n", SDL_GetError());
    return 0;
  }

  SDL_CreateWindowAndRenderer(SCREEN_WIDTH, SCREEN_HEIGHT, 0, &win, &ren);

  srand(time(NULL)); // seed the RNG using current time
  
  // fill the cells with random values
  for (int x = 0; x < COLS; x++) {
    for(int y = 0; y < ROWS; y++) {
      int choice = rand()%80;
      if (choice == 0) cells[x][y] = CELLTYPE_LIVE;
      else cells[x][y] = CELLTYPE_DEAD;
    }
  } 

  // update the cells until they reach a stable state
  while (iterations < MAX_ITERATIONS) {
		CellType newCells[COLS][ROWS];
		for (int x = blockOffset; x < COLS; x += 2) {
			for(int y = blockOffset; y < ROWS; y += 2) {
				int bottom = (y+1)%ROWS;
				int right = (x+1)%COLS;
				CellType* newBlock = doRule(cells, x, y);
	
				newCells[x][y] = newBlock[0];
				newCells[right][y] = newBlock[1];
				newCells[x][bottom] = newBlock[2];
				newCells[right][bottom] = newBlock[3];

				free(newBlock);
			}
		} 

    // update `cells`    
    memcpy(cells, newCells, sizeof(cells));

    // Technically we could increment `blockOffset`, but for 2x2 blocks alternating
    // it offset between 0 and 1 achieves the same result.
    blockOffset = blockOffset == 0? 1 : 0;

    iterations++;
  }

	// draw all the cells
	for (int x = 0; x < COLS; x++) {
		for(int y = 0; y < ROWS; y++) {
			CellType cState = cells[x][y];
			renderCell(cState, x, y, ren);
		}
	}

	SDL_RenderPresent(ren);
	
  // main loop
  while (1) {
    // exit main loop when window is closed
    if(SDL_PollEvent(&event) && event.type == SDL_QUIT) break;
  }

    
  // clean
  SDL_DestroyRenderer(ren);
  SDL_DestroyWindow(win);
  SDL_Quit();
  return 0;
}

void renderCell(CellType ctype, int x, int y, SDL_Renderer* ren) {
  SDL_Rect rect;
  rect.x = x*CELL_WIDTH;
  rect.y = y*CELL_HEIGHT;
  rect.w = CELL_WIDTH;
  rect.h = CELL_HEIGHT;

  // set color based on cell type
  switch (ctype) {
  case CELLTYPE_DEAD:
    SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
    break;
  case CELLTYPE_LIVE:
    SDL_SetRenderDrawColor(ren, 0, 200, 150, 255);
    break;
  }

  SDL_RenderFillRect(ren, &rect);
}

CellType* doRule(CellType cells[COLS][ROWS], int x, int y) {
  int bottom = (y+1)%ROWS;
  int right = (x+1)%COLS;
  CellType* buf = malloc(4);

  if (!buf) return NULL;
  
  // test to see if the cells are all the same
  if (cells[x][bottom] == cells[x][y]
      && cells[right][y] == cells[x][y]
      && cells[right][bottom] == cells[x][y]) {
    CellType oppositeCell;
    if (cells[x][y] == CELLTYPE_DEAD) oppositeCell = CELLTYPE_LIVE;
    else oppositeCell = CELLTYPE_DEAD;
    // if cells are all same, flip their state
    buf[0] = oppositeCell;
    buf[1] = oppositeCell;
    buf[2] = oppositeCell;
    buf[3] = oppositeCell;
  } else {
    // otherwise, keep the state the same
    buf[0] = cells[x][y];
    buf[1] = cells[right][y];
    buf[2] = cells[x][bottom];
    buf[3] = cells[right][bottom];
  }

  return buf;
}
